# Evaluación Tutorial 8. Full Driver

## Teoría

### Espacios existentes para el manejo y diseño de drivers

- **Espacio de usuario:**
<div style="text-align: justify"> Aplicaciones enfocadas al usuario, como los shells de UNIX, son parte del espacio de usuario. Estas aplicaciones no interaccionan directamente con el hardware, sino a través de funciones del kernel para tal fin, o llamadas al sistema (syscalls). La interacción con el hardware, en espacio de usuario, se realiza por medio de escritura y lecturas a archivos del sistema de archivos que provee el sistema operativo (para sistemas basados en UNIX). </div>


- **Espacio de kernel:**
<div style="text-align: justify"> El kernel maneja el hardware de una máquina de manera directa, simple y eficiente, brindando una interfaz de programación (API) uniforme. Cualquier subrutina o función que forma parte del kernel, como las encontradas en módulos y controladores de dispositivos, son considerados parte del espacio del kernel. La interacción con el hardware, en espacio de kernel, se realiza por medio de escritura y lecturas a direcciones de memoria, reservadas para el uso del hardware (mapas de memoria o puertos). </div>

### Implementación de funciones de un driver

- **En espacio de usuario:**
<div style="text-align: justify"> Cuando se carga un driver en el kernel, se suelen realizar algunas tareas preliminares, como reservar espacios de memoria, puertos de entrada/salida, interrupciones, etc. Estas tareas se llevan a cabo en espacio de kernel, por lo que deben ser implementadas por el desarrollador explı́citamente, para lo que se definen dos funciones en espacio de kernel: module init() y module exit(), que corresponden a los comandos en espacio de usuario insmod y rmmod respecticamente. Cuando el usuario ejecuta insmod o rmmod, el kernel debe llamar a module init() y module exit() según corresponda.
 </div>


- **En espacio de kernel:**
<div style="text-align: justify">
Se define una estructura estándar *file operations* que permite ligar las operaciones de espacio de usuario de abrir, cerrar, escribir y leer archivos con las funciones equivalentes en espacio de kernel como (memory init y memory exit), ası́ como para abrir (memory_open), cerrar (memory_close), escribir (memory_write) y leer (memory_read ).
</div>

### FYI
- **Abrir y Cerrar**
<div style="text-align: justify;text-indent: 50px">
Dentro del driver, debe crearse en el enlace correspondiente con el archivo de dispositivo (/dev/memory). Se utiliza la función register chrdev que toma tres argumentos: major, el nombre del módulo, y la estructura estándar file operations. Este enlace debe realizarse en la función de memory init. 
Para remover el driver del kernel dentro de la función memory_exit, se debe llamar a la función *unregister_chrdev*, que liberará el major del kernel.
</div>


- **Leer y Escribir**
<div style="text-align:justify;text-indent: 50px">La función memory_read toma argumentos: una estructura de tipo de archivo, un buffer (buf) que será retornado a la función de espacio de usuario (fopen, cat, etc), un contador (count) con la cantidad de bytes a transferir y la posición de la cuál empezar la lectura del archivo (f pos).</div>


## Práctica
  `Compiled for 4.4.0-122-generic`

### Compilar
    sudo make

### Interactuar
  ```
  sudo mknod /dev/memory c 60 0 //Generar "nodo" en /dev
  sudo chmod 666 /dev/memory  //Cambiar permisos
  sudo insmod memory.ko //Insertar módulo
  echo -n HOLA >/dev/memory //Escribir al módulo
  cat /dev/memory  //Leer del módulo
  ```
