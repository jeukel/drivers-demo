#!/bin/bash

HERE=`readlink -f .`

mkdir -p build/usr && cd build
cmake ../ -DCMAKE_INSTALL_PREFIX:PATH=$HERE/build/usr
make
